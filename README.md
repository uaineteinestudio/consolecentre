# ConsoleCentre

A c++ script for centering a line to the console in one simple call.

## Getting Started

Just copy the CentreText header and cpp files to your working directory and call.

See the [ConsoleCentre.cpp](ConsoleCentre/ConsoleCentre.cpp) file for execution of various problems and the levelmap.h and levelmap.cpp for an example execution.

## Authors

* **Daniel Stamer-Squair** - *UaineTeine*

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.